#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass IEEEtran
\begin_preamble
\usepackage[english]{babel} 
\usepackage{xparse}
\DeclareFontFamily{OT1}{pzc}{}
\DeclareFontShape{OT1}{pzc}{m}{it}{<-> s * [1.10] pzcmi7t}{}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}
\DeclareMathOperator*{\argmin}{argmin}  
%\newcommand{\vect}{\bf}
\newcommand{\vect}[1]{{\mathbf{#1}}}
\newcommand{\matr}{\bf}

\renewcommand{\footnotesize}{\fontsize{7.5pt}{9pt}\selectfont}
%\usepackage{enumitem}

\newcommand{\tl}[1]{{\color{orange} - Fix Language: #1 - \ }}  % Typo or 
%Language
\newcommand{\axx}[1]{{\color{blue} AT: #1  \ }}  % AmirHossein's comments
\newcommand{\dxx}[1]{{\color{green} DS: #1  \ }}  % Dylan's comments
\newcommand{\rxx}[1]{{\color{orange} RO: #1  \ }}  % Reza's comments
\newcommand{\sxx}[1]{{\color{red} SC: #1  \ }}  % Suman's comments
\newcommand{\nxx}[1]{#1}  % approved

\newcommand{\bxx}[1]{{\color{blue} - T: #1 - \ }}  % AmirHossein's comments
\newcommand{\XX}[3][2]{\mathbf{X}_{\tiny #2}^{\tiny #3}}
\newcommand{\pr}{\textrm{p}}
\newcommand{\pp}[3][2]{\pr(x #2 | #3)}

%\newcommand{\bIn}{\boldsymbol{I}_{\boldsymbol{n}}}
%\newcommand{\bIa}{\boldsymbol{I}_{\boldsymbol{a}}}
%\newcommand{\bIk}{\boldsymbol{I}_{\boldsymbol{k}}}
%\newcommand{\bIkk}{\boldsymbol{I}_{\boldsymbol{k-1}}}

\newcommand{\bIn}{\boldsymbol{I}_{{n}}}
\newcommand{\bIa}{\boldsymbol{I}_{{a}}}
\newcommand{\bIk}{\boldsymbol{I}_{{k}}}
\newcommand{\bIkk}{\boldsymbol{I}_{{k-1}}}
\newcommand{\bIs}[1]{\boldsymbol{I}_{#1}}  % Suman'
\newcommand{\xx}[3][2]{\mathbf{x}_{ #2}^{ #3}}
\newcommand{\zz}[3][2]{\mathbf{z}_{ #2}^{ #3}}
\newcommand{\ZZ}[3][2]{\mathbf{Z}_{ #2}^{ #3}}
\newcommand{\ZZh}[3][2]{\mathbf{\hat{z}}_{ #2}^{ #3}}
%\newcommand{\yy}[3][2]{\mathpzc{y}_{\tiny #2}^{\tiny #3}}
%\newcommand{\YY}[3][2]{\mathpzc{Y}_{\tiny #2}^{\tiny #3}}

\newcommand{\yy}[3][2]{\psi^{\tiny #3}({\tiny #2})}
\newcommand{\YY}[3][2]{\phi^{\tiny #3}({\tiny #2})}
\newcommand{\suf}[1]{\textsc{\tiny #1}}  % Suman's comments

\newcommand{\ii}[3][2]{\overline{\delta \mathpzc{i}}_{\tiny {#2}}^{\tiny {#3}}}
\newcommand{\II}[3][2]{\overline{\delta \mathpzc{I}}_{\tiny {#2}}^{\tiny {#3}}}
\newcommand{\psx}{p_*(\vect{x})}
\newcommand{\pcfx}{p_{\textsc{\tiny CF}}(\vect{x})}
\newcommand{\phybx}{p_{\textsc{\tiny HYB}}(\vect{x})}
\newcommand{\pixz}{\tilde{p}^i(\xx[]{k}{}\vert \vect{Z}_{k})}
\newcommand{\pixzm}{\tilde{p}^i(\xx[]{k}{}\vert \vect{Z}_{k-1})}
\newcommand{\pzix}{\tilde{p}^i( \vect{z}_{k} \vert \xx[]{k}{})}
\newcommand{ \pcf}{\frac{1}{\eta_{\textsuperscript{CF}}}\prod_{i=1}^{N} \pr(\vect{z}_{k}^i | \xx[]{k}{})^{\omega_i} \prod_{i=1}^{N} \tilde{p}^i(\vect{x}_{k} | \vect{Z}_{k-1})^{\omega_i}}
\newcommand{ \phyb}{\frac{1}{\eta_{\textsc{\tiny HYB}}}\prod_{i=1}^{N} \pr(\vect{z}_{k}^i | \xx[]{k}{}) \prod_{i=1}^{N} \tilde{p}^i(\vect{x}_{k} | \vect{Z}_{k-1})^{\bar{\omega}_i} }
\newcommand{ \pstar}{\frac{1}{\eta_*} \{ \prod_{i=1}^{N} \pr(\vect{z}_{k}^i | \xx[]{k}{}) \} \pr(\xx[]{k}{}|\vect{Z}_{k-1})}
 
\newcommand{ \npcf}{\prod_{i=1}^{N} \pr(\vect{z}_{k}^i | \xx[]{k}{})^{\omega_i} \prod_{i=1}^{N} \tilde{p}^i(\vect{x}_{k} | \vect{Z}_{k-1})^{\omega_i}}
\newcommand{ \nphyb}{\prod_{i=1}^{N} \pr(\vect{z}_{k}^i | \xx[]{k}{}) \prod_{i=1}^{N} \tilde{p}^i(\vect{x}_{k} | \vect{Z}_{k-1})^{\bar{\omega}_i} }
\newcommand{ \npstar}{ \{ \prod_{i=1}^{N} \pr(\vect{z}_{k}^i | \xx[]{k}{}) \} \pr(\xx[]{k}{}|\vect{Z}_{k-1})}
 
\DeclareDocumentCommand{\ais}{m m m  }{{#1}_{#2}^{#3}}
\DeclareDocumentCommand{\bis}{m m m  }{{#1}_{{\tiny #2}}^{{\tiny #3}}}
\DeclareDocumentCommand{\cis}{m m m  }{{#1}_{{\tiny #2}}^{{\tiny {#3}}}}
\newcommand{\ti}[1]{\textsc{\tiny #1}}
\newcommand{\lrp}[1]{\left( #1 \right) }
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\papersize default
\use_geometry false
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Distributed Estimation of GMMs on Unreliable Networks
\end_layout

\begin_layout Section
Preliminaries
\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\bIn}{\boldsymbol{I}_{n}}
{\boldsymbol{I}_{n}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\bIk}{\boldsymbol{I}_{k}}
{\boldsymbol{I}_{k}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\bIkk}{\boldsymbol{I}_{k-1}}
{\boldsymbol{I}_{k-1}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\XX}[2]{\mathbf{x}_{\tiny#1}^{\tiny#2}}
{\mathbf{x}_{#1}^{#2}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\zz}[2]{\mathbf{z}_{\tiny#1}^{\tiny#2}}
{\mathbf{z}_{#1}^{#2}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\vect}[1]{\mathbf{#1}}
{\mathbf{\mathbf{#1}}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\mat}[1]{\mathbf{#1}}
{#1}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\pr}{\textrm{p}}
{\textrm{p}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\pcfx}{\pr_{\textsc{\tiny CF}}(\vect x)}
{\pr_{CF}(\vect x)}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\II}[2]{\overline{{\delta\mathpzc{I}}}_{\tiny{#1}}^{\tiny{#2}}}
{\overline{\delta I}_{#1}^{#2}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\ii}[2]{\overline{{\delta\mathpzc{i}}}_{\tiny{#1}}^{\tiny{#2}}}
{\overline{\delta i}_{#1}^{#2}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\YY}[2]{\mathpzc{Y}_{\tiny{#1}}^{\tiny{#2}}}
{Y_{#1}^{#2}}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\yy}[2]{\mathpzc{y}_{\tiny{#1}}^{\tiny{#2}}}
{y_{#1}^{#2}}
\end_inset


\end_layout

\begin_layout Standard
Let 
\begin_inset Formula $\mathbf{x}\in\mathbb{X}\subset\mathbb{R}^{n_{x}}$
\end_inset

, 
\begin_inset Formula $\mathbf{u}\in\mathbb{U}\subset\mathbb{R}^{n_{u}}$
\end_inset

, and 
\begin_inset Formula $\mathbf{z}\in\mathbb{Z}\subset\mathbb{R}^{n_{z}}$
\end_inset

, denote the state, control and observation vectors, respectively for a
 stochastic system with motion and observation model
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\dot{\XX{}{}} & =f(\XX{}{},\mathbf{u})\\
\mathbf{z} & =h(\XX{}{})
\end{align}

\end_inset


\end_layout

\begin_layout Standard
In the rest of this document we consider linear observation model
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\mathbf{z} & =\mathbf{\mat H}\XX{}{}+\nu,\quad\mathcal{\nu\sim N}(\mathbf{\mat 0},\mat{\mat{\mathbf{R}}})
\end{align}

\end_inset


\end_layout

\begin_layout Standard
This is equivalent to 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\pr(\zz{}{}|\XX{}{})=\frac{1}{\sqrt{(2\pi)^{n}\left|\mathbf{\mat R}\right|}}\text{e}^{-\frac{1}{2}(\zz{}{}-\mathbf{\mat H}\XX{}{})^{T}\mathbf{R}^{-1}(\zz{}{}-\mathbf{\mat H}\XX{}{})}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
When the observer receives a concrete observation 
\begin_inset Formula $\zz{}*$
\end_inset

, it is sometimes helpful to use the so called canonical (Information) form
 of the observation model represented as
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\pr(\zz{}*|\XX{}{}) & =\frac{\text{e}^{-\frac{1}{2}(\zz{}*{}^{T}\mat R^{-1}\zz{}*)}}{\sqrt{(2\pi)^{n}\left|\mat R\right|}}\text{e}^{-\frac{1}{2}(\XX{}T(\mat H\mat R^{-1}\mat H)\XX{}{}-2\XX{}T(\mat H\mat R^{-1}\zz{}*))}\nonumber \\
 & =\eta_{\zz{}*}\text{e}^{-\frac{1}{2}(\XX{}T(\II{}*)\XX{}{}-2\XX{}T\ii{}*)}\label{eq:obs_inf}\\
 & =\text{e}^{-\frac{1}{2}(\gamma_{z}+\XX{}T(\II{}*)\XX{}{}-2\XX{}T\ii{}*)},
\end{align}

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $\II{}*$
\end_inset

 and 
\begin_inset Formula $\ii{}*$
\end_inset

 are called information matrix and information vector respectively.
 The observation dependent constants, 
\begin_inset Formula $\eta_{\zz{}*}$
\end_inset

 and 
\begin_inset Formula $\gamma_{z}$
\end_inset

 are immaterial in the fusion of a Gaussian prior (or prediction) and unimodal
 Gaussian observation.
 However, we will need them in some of the derivations below.
\end_layout

\begin_layout Standard
Similarly a prior (or prediction) has the canonical form
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\pr(\mathbf{\XX{}{}}) & =\frac{1}{\sqrt{(2\pi)^{n}\left|\mat{\Sigma}\right|}}\text{e}^{-\frac{1}{2}(\mathbf{\XX{}{}}-\vect{\mu})^{T}\mat{\Sigma}^{-1}(\mathbf{\XX{}{}}-\vect{\mu})}\\
 & =\frac{\text{e}^{-\frac{1}{2}(\vect{\mu}^{T}\mat{\Sigma}^{-1}\vect{\mu})}}{\sqrt{(2\pi)^{n}\left|\mat{\Sigma}\right|}}\text{e}^{-\frac{1}{2}(\XX{}T\mat{\Sigma}^{-1}\XX{}{}-2\XX{}T\mat{\Sigma}^{-1}\vect{\mu})}\\
 & =\eta_{\XX{}{}}\text{e}^{-\frac{1}{2}(\XX{}T(\YY{}{})\XX{}{}-2\XX{}T\yy{}{})}\label{eq:gaussian}\\
 & =\text{e}^{-\frac{1}{2}(\gamma+\XX{}T(\YY{}{})\XX{}{}-2\XX{}T\yy{}{})},
\end{align}

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $\YY{}{}=\mat{\Sigma}^{-1}$
\end_inset

 and 
\begin_inset Formula $\yy{}{}=\mat{\Sigma}^{-1}\vect{\mu}$
\end_inset

 are information vector and information matrix representation of prior (or
 prediction) information.
 The update step in information filtering becomes simple addition of canonical
 representations of prediction and observation.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\pr(\XX{}{}|\zz{}{}) & =\eta\pr(\XX{}{})\pr(\zz{}{}|\XX{}{})\\
 & =\eta\eta_{\zz{}*}\eta_{\XX{}{}}\text{e}^{-\frac{1}{2}(\XX{}T\YY{}{}\XX{}{}-2\XX{}T\yy{}{})}\text{e}^{-\frac{1}{2}(\XX{}T(\II{}*)\XX{}{}-2\XX{}T\ii{}*)}\\
 & \propto\text{e}^{-\frac{1}{2}(\XX{}T(\YY{}{}+\II{}*)\XX{}{}-2\XX{}T(\yy{}{}+\ii{}*))}
\end{align}

\end_inset


\end_layout

\begin_layout Standard
Accordingly if we have multiple observations we only write
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\YY{posterior}{} & =\YY{prediction}{}+\sum_{j=1}^{n}\II j*\\
\yy{posterior}{} & =\yy{prediction}{}+\sum_{j=1}^{n}\ii j{}
\end{align}

\end_inset


\end_layout

\begin_layout Remark
With a little abuse of notation we can represent the Gaussian distribution
 in Eq.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:gaussian"
plural "false"
caps "false"
noprefix "false"

\end_inset

 both in normal and canonical formats using a similar notation and let the
 reader distinguish the parameters based on the notation used in the rest
 of this paper.
 That is, we can represent the Gaussian distribution of Eq.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:gaussian"
plural "false"
caps "false"
noprefix "false"

\end_inset

 both as 
\begin_inset Formula $\mathcal{N}(\XX{}{};\vect{\mu},\mat{\Sigma})$
\end_inset

 and 
\begin_inset Formula $\mathcal{N}(\XX{}{};\yy{}{},\YY{}{})$
\end_inset

.
\end_layout

\begin_layout Standard

\end_layout

\begin_layout Remark
The likelihood function of Eq.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:obs_inf"
plural "false"
caps "false"
noprefix "false"

\end_inset

 is also proportional to a Gaussian function 
\begin_inset Formula $\mathcal{N}(\XX{}{};\ii{}{},\II{}{})$
\end_inset

, i.e., 
\end_layout

\begin_layout Remark
\begin_inset Formula 
\begin{equation}
\pr(\zz{}*|\XX{}{})\propto\mathcal{N}(\XX{}{};\ii{}{},\II{}{})
\end{equation}

\end_inset


\end_layout

\begin_layout Fact
The product of two Gaussian 
\begin_inset Formula $\pr_{1}=\mathcal{N}(\XX{}{};\vect{\mu}_{1},\mat{\Sigma}_{1})$
\end_inset

 and 
\begin_inset Formula $\pr_{2}=\mathcal{N}(\XX{}{};\vect{\mu}_{2},\mat{\Sigma}_{2})$
\end_inset

 is a scaled Gaussian.
 The canonical form is more suitable for calculations:
\end_layout

\begin_layout Fact
\begin_inset Formula 
\begin{align}
\pr & =\pr_{1}\pr_{2}=\text{e}^{-\frac{1}{2}(\gamma_{1}+\XX{}T(\YY 1{})\XX{}{}-2\XX{}T\yy 1{})}\text{e}^{-\frac{1}{2}(\gamma_{2}+\XX{}T(\YY 2{})\XX{}{}-2\XX{}T\yy 2{})}\\
 & =\text{e}^{-\frac{1}{2}(\gamma_{1}+\gamma_{2}+\XX{}T(\YY 1{}+\YY 2{})\XX{}{}-2\XX{}T(\yy 1{}+\yy 2{}))}\\
 & =\text{e}^{-\frac{1}{2}(\gamma_{1}+\gamma_{2}-\gamma_{1+2})}\text{e}^{-\frac{1}{2}(\gamma_{1+2}+\XX{}T(\YY 1{}+\YY 2{})\XX{}{}-2\XX{}T(\yy 1{}+\yy 2{}))}
\end{align}

\end_inset


\end_layout

\begin_layout Fact
in which 
\begin_inset Formula $\gamma_{1+2}$
\end_inset

 is the constant corresponding to a Gaussian distribution with canonical
 form 
\begin_inset Formula $\mathcal{N}(\XX{}{};\yy 1{}+\yy 2{},\YY 1{}+\YY 2{})$
\end_inset


\end_layout

\begin_layout Definition
The GMM distribution for a random variable 
\begin_inset Formula $\mathbf{x}$
\end_inset

 is defined as
\end_layout

\begin_layout Definition
\begin_inset Formula 
\begin{align}
\pr(\mathbf{\XX{}{}}) & =\sum_{i=1}^{M}\theta_{i}\mathcal{N}(x|\mathbf{\mathbf{\boldsymbol{\boldsymbol{\mu}}}}_{i},\mathbf{\boldsymbol{\Sigma}}_{i})=\sum_{i=1}^{M}\theta_{i}c_{i}\\
 & =\sum_{i=1}^{M}\theta_{i}\frac{1}{\sqrt{(2\pi)^{n}\left|\mathbf{\boldsymbol{\Sigma}}_{i}\right|}}\text{e}^{-\frac{1}{2}(\mathbf{\XX{}{}}-\boldsymbol{\mathbf{\mu}}_{i})^{T}\mathbf{\mathbf{\boldsymbol{\Sigma}}}_{i}^{-1}(\mathbf{\XX{}{}}-\mathbf{\boldsymbol{\mu}}_{i})}
\end{align}

\end_inset


\end_layout

\begin_layout Definition
where 
\begin_inset Formula $c_{i}=\mathcal{N}(\XX{}{};\vect{\mu}_{i},\mat{\Sigma}_{i})$
\end_inset

 and 
\begin_inset Formula $\theta_{i}$
\end_inset

 are i'th component and its weight respectively.
\end_layout

\begin_layout Claim
\begin_inset CommandInset label
LatexCommand label
name "claim:fusion_rule"

\end_inset

Now we can calculate the expression for fusion of a unimodal Gaussian observatio
n with a GMM prior
\end_layout

\begin_layout Claim
\begin_inset Formula 
\begin{align}
\pr & (\XX{}{}|\zz{}{})=\eta\pr(\XX{}{})\pr(\zz{}{}|\XX{}{})=\eta\sum_{i=1}^{M}\theta_{i}c_{i}^{+}\nonumber \\
c_{i}^{+} & =\text{e}^{-\frac{1}{2}(\gamma_{i}+\gamma_{z}-\gamma_{i+z})}\text{e}^{-\frac{1}{2}(\gamma_{i+z}+\XX{}T(\YY i{}+\II{}{})\XX{}{}-2\XX{}T(\yy i{}+\ii{}{}))}\\
\eta^{-1} & =\int\sum_{i=1}^{M}\theta_{i}\text{e}^{-\frac{1}{2}(\gamma_{i}+\gamma_{z}-\gamma_{i+z})}\text{e}^{-\frac{1}{2}(\gamma_{i+z}+\XX{}T(\YY i{}+\II{}{})\XX{}{}-2\XX{}T(\yy i{}+\ii{}{}))}dx\nonumber 
\end{align}

\end_inset


\end_layout

\begin_layout Remark
For an arbitrary prior (prediction) PDF and 
\begin_inset Formula $n$
\end_inset

 independent observations of the form 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:obs_inf"
plural "false"
caps "false"
noprefix "false"

\end_inset

, all the observations can be replaced by a single equivalent observation
 whose information representation is obtained from summation of information
 vectors/matrices of individual observations.
 
\end_layout

\begin_layout Remark
\begin_inset Formula 
\begin{align}
\pr(\XX{}{}|\zz 1{},\cdots,\zz n{}) & =\eta\pr(\XX{}{})\prod_{i=1}^{n}\pr(\zz i{}|\XX{}{})\\
 & =\eta\pr(\XX{}{})\pr(\zz{}{}|\XX{}{})
\end{align}

\end_inset


\end_layout

\begin_layout Remark
The implication of this fact is that in a DSE (Distributed State Estimation)
 application where agents share the same prior and only observe independent
 observations, we can use consensus method as unimodal case to reach a consensus
 over collective information in the observations and then locally calculate
 posteriors for each agent according to the fusion rule discussed in 
\begin_inset CommandInset ref
LatexCommand ref
reference "claim:fusion_rule"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
 For this particular setup there is absolutely no need to communicate any
 thing other than the canonical form of the observations.
 All other constants can be calculated on site for each agent.
\end_layout

\begin_layout Section
Distributed State Estimation (DSE)
\end_layout

\begin_layout Standard
DSE in its most general form expressed as finding the posterior over random
 variable 
\begin_inset Formula $\XX{}{}$
\end_inset

 given the observation collected by nodes on a network.
 
\end_layout

\begin_layout Standard

\series bold
\begin_inset Formula 
\begin{align}
 & \pr(\XX k{}|\vect z_{k})=\frac{1}{\eta}\pr(\vect z_{k}|\XX k{})\pr(\XX k{}|\vect z_{k-1},\XX{k-1}{})\\
 & =\frac{1}{\eta}\prod_{i=1}^{n}\pr(\vect z_{k}^{i}|\XX k{})\int\pr(\XX{_{k}}{}|\XX{k-1}{})\pr(\XX{k-1}{}|\vect z_{k-1})\text{d}\XX{k-1}{}
\end{align}

\end_inset


\end_layout

\begin_layout Standard
We use the following notations
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\pr(\XX{k-1}{}|\zz{1:k-1}G) & \triangleq\pr(\XX{k-1}{}|\{\zz ki\}_{k\in\bIkk}^{i\in\bIn})\\
\pr(\XX k{}|\zz{1:k-1}G,\XX{k-1}{}) & \triangleq\pr(\XX k{}|\{\zz ki\}_{k\in\bIkk}^{i\in\bIn},\XX{k-1}{})\\
\pr(\XX{k-1}{}|\zz{1:k}G) & \triangleq\pr(\XX k{}|\{\zz ki\}_{k\in\bIk}^{i\in\bIn})
\end{align}

\end_inset


\end_layout

\begin_layout Subsection*
DSE in Functional Space
\end_layout

\begin_layout Standard
Given 
\begin_inset Formula $n$
\end_inset

 connected agents where each agent has prior 
\begin_inset Formula $\pr_{i}\triangleq\pr_{i}(\XX k{}|\zz{1:k-1}{G_{i}},\XX{k-1}{})\approx\pr(\XX k{}|\zz{1:k}G,\XX{k-1}{})$
\end_inset

 and makes an independent observation 
\begin_inset Formula $\zz ki$
\end_inset

 with likelihood 
\begin_inset Formula $\pr(\vect z_{k}^{i}|\XX k{})$
\end_inset

.
 We want to solve the problem
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\pr(\XX k{}|\zz{1:k}G,\XX{k-1}{})=\frac{1}{\eta}\pr(\XX k{}|\zz{1:k-1}G,\XX{k-1}{})\prod_{i=1}^{n}\pr(\vect z_{k}^{i}|\XX k{})
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
One way of obtaining a distributed approximation of this PDF is to first
 obtain a conservative fusion of priors as
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\bar{\pr}(\XX k{}|\zz{1:k}G,\XX{k-1}{})=\frac{1}{\eta}\prod_{i=1}^{n}\pr_{i}^{w_{i}}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
and reach a consensus over product of likelihoods 
\begin_inset Formula $\bar{\pr}(\vect z_{k}|\XX k{})$
\end_inset

.
 Then the estimate is 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\pr_{i}(\XX k{}|\zz{1:k}G,\XX{k-1}{})=\frac{1}{\eta_{1}}\bar{\pr}(\XX k{}|\zz{1:k}G,\XX{k-1}{})\bar{\pr}(\vect z_{k}|\XX k{})
\end{equation}

\end_inset


\end_layout

\begin_layout Section
GMM background related to DSE
\end_layout

\begin_layout Remark
Product of two GMMs 
\begin_inset Formula $\pr_{1}=\sum_{i=1}^{M}\theta_{1,i}c_{1,i}$
\end_inset

 and 
\begin_inset Formula $\pr_{2}=\sum_{i=1}^{M}\theta_{2,i}c_{2,i}$
\end_inset

 has the form of 
\end_layout

\begin_layout Remark
\begin_inset Formula 
\begin{equation}
\pr_{1}\pr_{2}=\sum_{i,j}f(c_{1,i},c_{2,j})\theta_{1,i}\theta_{2,j}c_{1,i}c_{2,j}
\end{equation}

\end_inset


\end_layout

\begin_layout Remark
where
\end_layout

\begin_layout Remark
\begin_inset Formula 
\begin{equation}
f_{i,j}\triangleq f(c_{1,i},c_{2,j})=\mathcal{N}(x=0|\mathbf{\mathbf{\boldsymbol{\boldsymbol{\mu}}}}_{1,i}-\mathbf{\mathbf{\boldsymbol{\boldsymbol{\mu}}}}_{1,j},\mathbf{\boldsymbol{\Sigma}}_{i}+\mathbf{\boldsymbol{\Sigma}}_{j})\label{eq:prod_f}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The above expression is useful for making approximations in GMM applications.
 For example for 
\begin_inset Formula $\mathbf{\mathbf{\boldsymbol{\boldsymbol{\mu}}}}_{1,i}\gg\mathbf{\mathbf{\boldsymbol{\boldsymbol{\mu}}}}_{1,j}$
\end_inset

.
 This means that 
\begin_inset Formula $f(c_{1,i},c_{2,j})$
\end_inset

 will have a very small value and we can safely drop the product term 
\begin_inset Formula $f(c_{1,i},c_{2,j})\theta_{1,i}\theta_{2,j}c_{1,i}c_{2,j}$
\end_inset

 and the normalization constant of the result remains very close to the
 original value 
\begin_inset Formula $\int\pr_{1}\pr_{2}dx$
\end_inset

.
 This approximation is tied to the concept of c-separation which is defined
 in the wiki of the project.
 Note that 
\begin_inset Formula $f(c_{1,i},c_{2,j})$
\end_inset

 can be calculated locally given individual components.
 Therefore, in distributed calculations that we will do later, there is
 no need to transfer it.
 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\pr_{1}\pr_{2} & =\sum_{i,j\in A}f_{i,j}\theta_{1,i}\theta_{2,j}c_{1,i}c_{2,j}+\sum_{k,l\in B}f_{k,l}\theta_{1,k}\theta_{2,l}c_{1,k}c_{2,l}\\
 & \approx\sum_{i,j\in A}f_{i,j}\theta_{1,i}\theta_{2,j}c_{1,i}c_{2,j}+\sum_{k,l\in B}\epsilon\theta_{1,k}\theta_{2,l}c_{1,k}c_{2,l}\\
 & \propto\sum_{i,j\in A}f_{i,j}\theta_{1,i}\theta_{2,j}c_{1,i}c_{2,j}
\end{align}

\end_inset


\end_layout

\end_body
\end_document
