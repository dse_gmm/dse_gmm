
# coding: utf-8

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

# In[4]:


get_ipython().run_line_magic('matplotlib', 'inline')

import matplotlib.pyplot as plt
import seaborn; seaborn.set_style('whitegrid')
import numpy

from pomegranate import *
d1 = NormalDistribution(5, 2)
d2 = NormalDistribution(1, 1)
model = GeneralMixtureModel([d1, d2], weights=[0.25, 0.75] )
x = numpy.arange(0, 10.01, 0.05)
plt.figure(figsize=(28, 6))
plt.subplot(121)
plt.title("~Norm(4, 1) + ~Norm(7, 1)", fontsize=14)
plt.ylabel("Probability Density", fontsize=14)
plt.fill_between(x, 0, model.probability(x))
plt.ylim(0, 1)


# In[18]:


get_ipython().run_line_magic('matplotlib', 'inline')

import matplotlib.pyplot as plt
import seaborn; seaborn.set_style('whitegrid')
import numpy

from pomegranate import *
d1 = NormalDistribution(5, 2)
d2 = NormalDistribution(1, 1)
weights=[0.25, 0.75]
model = GeneralMixtureModel([d1, d2], weights )
x = numpy.arange(-10.01, 10.01, 0.05)
plt.figure(figsize=(28, 6))
plt.subplot(121)
plt.title("~Norm(4, 1) + ~Norm(7, 1)", fontsize=14)
plt.ylabel("Probability Density", fontsize=14)
plt.fill_between(x, 0, model.probability(x),alpha=.3)
# plt.plot(x, model.probability(x))
plt.plot(x, weights[0]*(model.distributions[0].probability(x)), label="Distribution 1")
plt.plot(x, weights[1]*(model.distributions[1].probability(x)), label="Distribution 2")
# plt.plot(x, model.probability(x))

plt.ylim(0, 1)


# In[84]:


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
from pomegranate import *
d1 = MultivariateGaussianDistribution([1, 2], [[1, 0], [0, 1]])
d2 = MultivariateGaussianDistribution([2, 3], [[1, 0], [0, 1]])
d3 = MultivariateGaussianDistribution([1, 3], [[2, 0], [0, 3]])
model = GeneralMixtureModel([d1, d2, d3], weights=[0.25, 0.60, 0.15])
# df = pd.DataFrame(data, columns=["x", "y"])
# Our 2-dimensional distribution will be over variables X and Y
x = numpy.arange(-6, 6.1, .1)
y = numpy.arange(-6, 6.1, .1)

xx, yy = numpy.meshgrid(x, y)
x_ = numpy.array(list(zip(xx.flatten(), yy.flatten())))
p3 = model.probability(x_).reshape(len(x), len(y))
plt.figure(figsize=(14, 6))
plt.contourf(xx, yy, p3, cmap='Blues', alpha=0.8)


# In[81]:


x = numpy.arange(-6, 6.1, .1)
y = numpy.arange(-6, 6.1, .1)

xx, yy = numpy.meshgrid(x, y)
x_ = numpy.array(list(zip(xx.flatten(), yy.flatten())))
p2 = model.probability(x_).reshape(len(x), len(y))
plt.figure(figsize=(14, 6))
plt.contourf(xx, yy, p2, cmap='Blues', alpha=0.8)

